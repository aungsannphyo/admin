@extends('layout.master')

@section('title','Register')

@section('content')
	<div class="contaier">
		<div class="col-md-8 col-md-offset-2">
			<div class="well well bs-component">
				<legend>Register</legend>
				@foreach($errors->all() as $error)
				<p class="alert alert-danger">{{ $error }}</p>
				@endforeach
				<form method="post">

					{{csrf_field()}}

					<div class="form-group">
    <label for="Username">Username</label>
    <input type="text" class="form-control" name="name" placeholder="Username">
  </div>

  <div class="form-group">
    <label for="Email1">Email address</label>
    <input type="email" class="form-control" name="email" placeholder="Email">
  </div>

  <div class="form-group">
    <label for="Password">Password</label>
    <input type="password" class="form-control" name="password">
  </div>

  <div class="form-group">
    <label for="Confirm Password">Confirm Password</label>
    <input type="password" class="form-control" name="password_confirmation">
  </div>

  <button type="submit" class="btn btn-primary">Register</button>
  
</form>
			</div>
		</div>
	</div>
@endsection