@extends('layout.master')

@section('title','Login')

@section('content')

    <div class="contaier">
    <div class="col-md-8 col-md-offset-2">
      <div class="well well bs-component">

        <form method="post">
          <legend>Login</legend>

          {{csrf_field()}}
          @foreach($errors->all() as $error)
          <p class="alert alert-danger">{{ $error }}</p>
          @endforeach
  <div class="form-group">
    <label for="Email1">Email address</label>
    <input type="email" class="form-control" name="email" id="exampleInputEmail1" placeholder="Email">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Password</label>
    <input type="password" class="form-control" name="password">
  </div>
  <button type="submit" class="btn btn-default">Submit</button>
</form>

      </div>
    </div>
  </div>
@endsection